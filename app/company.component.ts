/**
 * Created by dmacho on 05.06.17.
 */
import {Component} from 'angular2/core';
import {Company} from './company';

@Component({
    selector: 'single-company',
    inputs: ['company'],
    host: {
        class: 'singlecompany'
    },
    templateUrl: 'app/company.component.html',

})

export class CompanyComponent{
    company: Company;
}