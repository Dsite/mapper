import  { Component } from 'angular2/core';
import  { OnInit } from 'angular2/core';
import  { NgFor, FORM_DIRECTIVES, FormBuilder, Validators, ControlGroup, Control, NgClass} from 'angular2/common';
import  { Company } from './company';
import  { CompanyComponent } from './company.component';
import  { CompanyService } from './company.service';

@Component({
	selector: 'companies',
	directives: [CompanyComponent, FORM_DIRECTIVES, NgClass],
	providers: [CompanyService],
	templateUrl:  'app/app.component.html'		//'<div> Dziś jest {{ day }} ! </div>'
})

export class AppComponent implements OnInit {
	
	companies: Company[];
	companyForm: ControlGroup;

	selectedCompany: Company;

	name: Control = new Control("", Validators.required);
	email: Control = new Control("", Validators.required);
	description: Control = new Control("", Validators.required);
	telNumber: Control = new Control("", Validators.required);


	constructor(private _companyService: CompanyService, fb: FormBuilder){
		this.companyForm = fb.group({
			'name': this.name,
			'email' : this.email,
			'description' : this.description,
			'telNumber' : this.telNumber,
				
		});
	}

	getCompanies(){
		this._companyService.getCompanies().then(companies => this.companies = companies);
	}

	ngOnInit(){
		this.getCompanies();
	}

	onSubmit(){
		this.companies.push(new Company(this.name.value, this.email.value, this.description.value, this.telNumber.value));
		this.name.updateValue("");
		this.email.updateValue("");
		this.description.updateValue("");
		this.telNumber.updateValue("");
			
	}

	onSelect(company: Company, event){
		event.preventDefault();
		this.selectedCompany = company;
	}
}

/*product.id = 1;
product.title = "kurs";
product.price = 67;
product.showProduct();*/