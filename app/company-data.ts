/**
 * Created by dmacho on 05.06.17.
 */
import  { Company } from './company';


export var COMPANIES: Company[] = [
    new Company('CompanyName1', 'email@ex.ru', 'designers', '7655435'),
    new Company('CompanyName2', 'email@ex.de', 'designers2', '5435654')
];