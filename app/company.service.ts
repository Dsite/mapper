/**
 * Created by dmacho on 05.06.17.
 */
import {COMPANIES} from './company-data';
import {Injectable} from 'angular2/core';

@Injectable()

export class CompanyService {
    getCompanies(){
        return Promise.resolve(COMPANIES);
    }
}