System.register(['./company'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var company_1;
    var COMPANIES;
    return {
        setters:[
            function (company_1_1) {
                company_1 = company_1_1;
            }],
        execute: function() {
            exports_1("COMPANIES", COMPANIES = [
                new company_1.Company('CompanyName1', 'email@ex.ru', 'designers', '7655435'),
                new company_1.Company('CompanyName2', 'email@ex.de', 'designers2', '5435654')
            ]);
        }
    }
});
//# sourceMappingURL=company-data.js.map