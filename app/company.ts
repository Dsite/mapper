/**
 * Created by dmacho on 05.06.17.
 */
export class Company{
    name: string;
    email: string;
    description: string;
    telNumber: string;


    constructor(name: string, email: string, telNumber: string, description?: string){ //votes?: oznacza nieobowizakowy parapetr
        this.name = name;
        this.email = email;
        this.description = description || "";
        this.telNumber = telNumber;
       // this.votes = votes || 0; // domyslna wartosc 0
    }
}