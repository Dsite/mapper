System.register(['angular2/core', 'angular2/common', './company', './company.component', './company.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, common_1, company_1, company_component_1, company_service_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (company_1_1) {
                company_1 = company_1_1;
            },
            function (company_component_1_1) {
                company_component_1 = company_component_1_1;
            },
            function (company_service_1_1) {
                company_service_1 = company_service_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(_companyService, fb) {
                    this._companyService = _companyService;
                    this.name = new common_1.Control("", common_1.Validators.required);
                    this.email = new common_1.Control("", common_1.Validators.required);
                    this.description = new common_1.Control("", common_1.Validators.required);
                    this.telNumber = new common_1.Control("", common_1.Validators.required);
                    this.companyForm = fb.group({
                        'name': this.name,
                        'email': this.email,
                        'description': this.description,
                        'telNumber': this.telNumber,
                    });
                }
                AppComponent.prototype.getCompanies = function () {
                    var _this = this;
                    this._companyService.getCompanies().then(function (companies) { return _this.companies = companies; });
                };
                AppComponent.prototype.ngOnInit = function () {
                    this.getCompanies();
                };
                AppComponent.prototype.onSubmit = function () {
                    this.companies.push(new company_1.Company(this.name.value, this.email.value, this.description.value, this.telNumber.value));
                    this.name.updateValue("");
                    this.email.updateValue("");
                    this.description.updateValue("");
                    this.telNumber.updateValue("");
                };
                AppComponent.prototype.onSelect = function (company, event) {
                    event.preventDefault();
                    this.selectedCompany = company;
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'companies',
                        directives: [company_component_1.CompanyComponent, common_1.FORM_DIRECTIVES, common_1.NgClass],
                        providers: [company_service_1.CompanyService],
                        templateUrl: 'app/app.component.html' //'<div> Dziś jest {{ day }} ! </div>'
                    }), 
                    __metadata('design:paramtypes', [company_service_1.CompanyService, common_1.FormBuilder])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
/*product.id = 1;
product.title = "kurs";
product.price = 67;
product.showProduct();*/ 
//# sourceMappingURL=app.component.js.map