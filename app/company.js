System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Company;
    return {
        setters:[],
        execute: function() {
            /**
             * Created by dmacho on 05.06.17.
             */
            Company = (function () {
                function Company(name, email, telNumber, description) {
                    this.name = name;
                    this.email = email;
                    this.description = description || "";
                    this.telNumber = telNumber;
                    // this.votes = votes || 0; // domyslna wartosc 0
                }
                return Company;
            }());
            exports_1("Company", Company);
        }
    }
});
//# sourceMappingURL=company.js.map